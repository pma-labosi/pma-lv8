package com.example.lv8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    private lateinit var btnF1: Button
    private lateinit var btnF2: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val currentFragment = supportFragmentManager.findFragmentById(R.id.FragmentContainer)
        if (currentFragment == null) {
            val fragment = Fragment1()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.FragmentContainer, fragment)
                .commit()
        }

        btnF1 = findViewById(R.id.button)
        btnF2 = findViewById(R.id.button2)

        btnF1.setOnClickListener {
            val fragment = Fragment1()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.FragmentContainer, fragment)
                .commit()
        }
        btnF2.setOnClickListener {
            val fragment = Fragment2()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.FragmentContainer, fragment)
                .commit()
        }
    }
}